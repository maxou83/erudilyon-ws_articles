package cpe.projet.erudilyon.wsstore;

import cpe.projet.erudilyon.wsarticles.ErudiLyonWsArticlesApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = ErudiLyonWsArticlesApplication.class)
@DataJpaTest
class ErudiLyonWsArticlesApplicationTests {

    @Test
    void contextLoads() {
    }

}
