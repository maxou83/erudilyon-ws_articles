package cpe.projet.erudilyon.wsarticles.Model;

import lombok.Data;
import org.checkerframework.checker.units.qual.C;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_E_ARTICLE_ARTICLE")
@Data
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ARTICLE_ID")
    private Integer id;

    @Column(name = "NOM_ARTICLE")
    private String nomArticle;

    @Column(name ="DESC_ARTICLE")
    private String descArticle;

    @Column(name = "IMG_ARTICLE")
    private String imgArticle;

    @Column(name = "PRIX_ARTICLE")
    private BigDecimal prixArticle;

    @Column(name = "QTE_ARTICLE")
    private Integer qteArticle;

    @Column(name = "DATE_DEBUT_ARTICLE")
    private Date dateDebutArticle;

    @Column(name = "DATE_FIN_ARTICLE")
    private Date dateFinArticle;

    @Column(name = "ETAT_ARTICLE", columnDefinition = "smallint default 1")
    private Integer etatArticle;

    @Column(name = "USER_ARTICLE")
    private Integer idUser;
}
