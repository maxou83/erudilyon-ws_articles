package cpe.projet.erudilyon.wsarticles.Controller;

import Model.ArticleDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsarticles.Model.Article;
import cpe.projet.erudilyon.wsarticles.Service.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin
@RestController
@Api(value = "API de gestion des Articles")
public class ArticleController {

    @Autowired
    ArticleService articleService;

    @ApiOperation(value = "Permet de controler la disponibilité du service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/up")
    public String up() {
        return "UP";
    }

    @ApiOperation(value = "Récupère tous les articles")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    @GetMapping("/all")
    public List<ArticleDTO> getAllArticles() {
        return articleService.getAllArticles();
    }

    @ApiOperation(value = "Récupère tous les articles en fonction d'un partenaire")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    @GetMapping("/articleByPartner")
    public List<ArticleDTO> getAllArticlesByPartner(@RequestBody UserDTO userDTO) {
        return articleService.getAllArticlesByPartner(userDTO);
    }

    @ApiOperation(value = "Récupère un article dont l'id est passé en paramètre")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    @GetMapping("/get/{id}")
    public ArticleDTO getArticle(@PathVariable Integer id) {
        return articleService.getArticle(id);
    }

    @ApiOperation(value = "Récupère tous les articles actifs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostAuthorize("hasAnyRole('USER','ADMIN','PARTNER')")
    @GetMapping("/all/enabled")
    public List<ArticleDTO> getAllEnabledArticles() {
        return articleService.getAllEnabledArticles();
    }


    @ApiOperation(value = "Ajoute une activité", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PostMapping("/add")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation addArticle(@RequestBody ArticleDTO articleDTO) {
        return articleService.addArticle(articleDTO);
    }


    @ApiOperation(value = "Modifie une activité", response = DataValidation.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @PutMapping("/edit")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation editArticle(@RequestBody ArticleDTO articleDTO) {

        return articleService.editArticle(articleDTO);
    }
}
