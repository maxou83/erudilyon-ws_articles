package cpe.projet.erudilyon.wsarticles.Service;

import Model.ArticleDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsarticles.Model.Article;
import cpe.projet.erudilyon.wsarticles.Repository.ArticleRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.swagger2.mappers.ModelMapper;

import javax.validation.constraints.Null;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    public List<ArticleDTO> getAllArticles(){
        List<Article> articleList = new ArrayList<>();
        List<ArticleDTO> articleResultList = new ArrayList<>();
        articleRepository.findAll().forEach(articleList::add);
        for (Article article: articleList) {

            ArticleDTO activityDTO = this.transformToDTO(article);

            articleResultList.add(activityDTO);
        }
        return articleResultList;
    }

    public List<ArticleDTO> getAllArticlesByPartner(UserDTO userDTO){
        List<Article> articleList = new ArrayList<>();
        List<ArticleDTO> articleResultList = new ArrayList<>();
        articleRepository.findAllByIdUser(userDTO.getUserId()).forEach(articleList::add);
        for (Article article: articleList) {

            ArticleDTO activityDTO = this.transformToDTO(article);

            articleResultList.add(activityDTO);
        }
        return articleResultList;
    }

    public List<ArticleDTO> getAllEnabledArticles(){
        List<Article> articleList = new ArrayList<>();
        List<ArticleDTO> articleResultList = new ArrayList<>();
        articleRepository.findAllByEtatArticleEquals(1).forEach(articleList::add);
        for (Article article: articleList) {

            ArticleDTO activityDTO = this.transformToDTO(article);

            articleResultList.add(activityDTO);
        }
        return articleResultList;
    }

    public ArticleDTO getArticle(Integer idArticle){
        return this.transformToDTO(articleRepository.findById(idArticle).get());
    }

    public DataValidation editArticle(ArticleDTO articleDTO){
        DataValidation dataValidation = new DataValidation();
        dataValidation.setElem(Article.class.getName());
        try{
            Article article = this.transformToDAO(articleDTO);
            articleRepository.save(article);
        }catch (Exception e){
            dataValidation.setStatus(e + " MANQUANT");
            dataValidation.setValid(false);
            return dataValidation;
        }

        dataValidation.setValid(true);
        return dataValidation;
    }

    public DataValidation addArticle(ArticleDTO articleDTO){
        return editArticle(articleDTO);
    }

    private ArticleDTO transformToDTO(Article article){
        ArticleDTO articleDTO = new ArticleDTO();

        articleDTO.setId(article.getId());
        articleDTO.setNomArticle(article.getNomArticle());
        articleDTO.setDescArticle(article.getDescArticle());
        articleDTO.setImgArticle(article.getImgArticle());
        articleDTO.setPrixArticle(article.getPrixArticle());
        articleDTO.setQteArticle(article.getQteArticle());
        articleDTO.setDateDebutArticle(article.getDateDebutArticle());
        articleDTO.setDateFinArticle(article.getDateFinArticle());
        articleDTO.setEtatArticle(article.getEtatArticle());
        articleDTO.setIdUser(article.getIdUser());
        return articleDTO;

    }

    private Article transformToDAO(ArticleDTO articleDTO) throws Exception{
        Article article = new Article();

        article.setId(articleDTO.getId());

        if(StringUtils.isBlank(articleDTO.getNomArticle())) throw new NullPointerException("NOM");
        if(StringUtils.isBlank(articleDTO.getDescArticle())) throw new NullPointerException("DESC");
        article.setNomArticle(articleDTO.getNomArticle());
        article.setDescArticle(articleDTO.getDescArticle());

        if(StringUtils.isBlank(articleDTO.getImgArticle())){
            article.setImgArticle("https://birkeland.uib.no/wp-content/themes/bcss/images/no.png");
        } else {
            article.setImgArticle(articleDTO.getImgArticle());
        }

        if(articleDTO.getPrixArticle() == null){
            throw new NullPointerException("PRIX");
        } else {
            article.setPrixArticle(articleDTO.getPrixArticle());
        }

        if(articleDTO.getQteArticle() == null){
            throw new NullPointerException("QTE");
        } else {
            article.setQteArticle(articleDTO.getQteArticle());
        }

        if(null == articleDTO.getDateDebutArticle()){
            throw new NullPointerException("DATE DEBUT");
        }
        if(null == articleDTO.getDateFinArticle()){
            throw new NullPointerException("DATE FIN");
        }

        article.setDateDebutArticle(articleDTO.getDateDebutArticle());
        article.setDateFinArticle(articleDTO.getDateFinArticle());

        if(articleDTO.getEtatArticle() == null){
            article.setEtatArticle(articleDTO.getEtatArticle());
        } else {
            article.setEtatArticle(articleDTO.getEtatArticle());
        }

        if(articleDTO.getIdUser() == null) {
            throw new NullPointerException("USER");
        } else {
            article.setIdUser(articleDTO.getIdUser());
        }

        return article;
    }

}
