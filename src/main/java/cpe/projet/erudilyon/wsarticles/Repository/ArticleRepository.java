package cpe.projet.erudilyon.wsarticles.Repository;

import cpe.projet.erudilyon.wsarticles.Model.Article;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ArticleRepository extends CrudRepository<Article, Integer> {

    List<Article> findAllByEtatArticleEquals(Integer value);

    List<Article> findAllByIdUser(Integer idUser);


}
